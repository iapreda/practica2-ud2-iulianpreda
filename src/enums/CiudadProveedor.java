package enums;


/**
 * Clase que rellena JComboBox de ciudad de Proveedores de la clase Vista.
 * @see interfaz.Vista
 */
public enum CiudadProveedor {

    ALAVA("Alava"),
    ALBACETE("Albacete"),
    ALICANTE("Alicante"),
    ALMERIA("Almeria"),
    ASTURAS("Asturias"),
    AVILA("Avila"),
    BADAJOZ("Badajoz"),
    BARCELONA("Barcelona"),
    BURGOS("Burgos"),
    CACERES("Cáceres"),
    CADIZ("Cádiz"),
    CANTABRIA("Cantabria"),
    CASTELLON("Castellon"),
    CIUDADREAL("Ciudad Real"),
    CORDOBA("Córdoba"),
    LACORUNA("La Coruña"),
    CUENCA("Cuenca"),
    GERONA("Gerona"),
    GRANADA("Granada"),
    GUADALAJARA("Guadalajara"),
    GUIPUZCOA("Guipúzcoa"),
    HUELVA("Huelva"),
    HUECA("Huesca"),
    ISLASBALEARES("Islas Baleares"),
    JAEN("Jaén"),
    LEON("León"),
    LERIDA("Lérida"),
    LUGO("Lugo"),
    MADRID("Madrid"),
    MALAGA("Málaga"),
    MURCIA("Murcia"),
    NAVARRA("Navarra"),
    OURENSE("Ourense"),
    PALENCIA("Palencia"),
    PONTEVEDRA("Pontevedra"),
    LARIOJA("La Rioja"),
    SALAMANCA("Salamanca"),
    SEGOVIA("Segovia"),
    SEVILLA("Sevilla"),
    SORIA("Soria"),
    TARRAGONA("Tarragona"),
    SANTACRUZDETENERIFE("Santa Cruz de Tenerife"),
    TERUEL("Teruel"),
    TOLEDO("Toledo"),
    VALENCIA("Valencia"),
    VALLADOLID("Valladolid"),
    VIZCAYA("Vizcaya"),
    ZAMORA("Zamora"),
    ZARAGOZA("Zaragoza");



    private String valor;

    CiudadProveedor(String valor){this.valor = valor;}

    public String getValor() {
        return valor;
    }
}
