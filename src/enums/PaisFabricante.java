package enums;

/**
 * Clase que rellena JComboBox de Pais de fabricante en el formulario Vista.
 *
 * @see interfaz.Vista
 */

public enum PaisFabricante {

    ALEMANIA("Alemania"),
    AUSTRIA("Austria"),
    BELGICA("Bélgica"),
    BULGARIA("Bulgaria"),
    CHIPRE("Chipre"),
    CROACIA("Croacia"),
    DINAMARCA("Dinamarca"),
    ESLOVAQUIA("Eslovaquia"),
    ESLOVENIA("Eslovenia"),
    ESPANA("España"),
    ESTADOSUNIDOS("Estados Unidos"),
    ESTONIA("Estonia"),
    FINLANDIA("Finlandia"),
    FRANCIA("Francia"),
    GRECIA("Grecia"),
    HUNGRIA("Hungria"),
    IRLANDA("Irlanda"),
    ITALIA("Italia"),
    LETONIA("Letonia"),
    LITUANIA("Lituania"),
    LUXEMBURGO("Luxemburgo"),
    MALTA("Malta"),
    PAISESBAJOS("Paises Bajos"),
    POLONIA("Polonia"),
    PORTUGAL("Portugal"),
    REPUBLICACHECA("Republica Checa"),
    RUMANIA("Rumania"),
    SUECIA("Suecia");


    private String valor;
    PaisFabricante(String valor){this.valor = valor;}

    public String getValor() {
        return valor;
    }
}
