package interfaz;

import com.github.lgooddatepicker.components.DatePicker;
import enums.CiudadProveedor;
import enums.PaisFabricante;


import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

/**
 * Clase publica de vista
 */
public class Vista extends JFrame{

    private JPanel panel1;
    private JTabbedPane tabbedPane;

    // COMPONENTES PANEL TIENDA
    JPanel panelTienda;
    JTextField txModeloTienda;
    JTextField txProcesadorTienda;
    JTextField txRamTienda;
    JTextField txPlacaTienda;
    JTextField txPrecioTienda;
    JTextField txNumSerie;
    JComboBox<String> cbMarcaTienda;
    JTable tablaTienda;
    JButton btnAddTienda;
    JButton btnEliminarTienda;
    JButton btnModificarTienda;
    JComboBox<String> cbProveedor;

    // COMPONENTES PANEL PROVEEDOR
    JPanel panelProveedor;
    JTextField txNombreProveedor;
    JTextField txCIFProveedor;
    JTextField txDirProveedor;
    JTextField txTelefonoProveedor;
    JTextField txCorreoProveedor;
    JTextField txDescuentoProveedor;
    JTable tablaProveedor;
    DatePicker dpEntradaTienda;
    JButton btnAddProveedor;
    JButton btnEliminarProveedor;
    JButton btnModificarProveedor;
    DatePicker dpProveedor;
    JComboBox<String> cbCiudadProveedor;

    // COMPONENTES PANEL FABRICANTE
    JPanel panelFabricante;
    JTextField txEmpresaFabricante;
    JTextField txTlfFabricante;
    JTextField txCIFFabricante;
    JComboBox<String> cbPaisFabricante;
    JButton btnAddFabricante;
    JButton btnModificarFabricante;
    JButton btnEliminarFabricante;
    JTable tablaFabricante;
    JTextField txMailFabricante;
    JTextField txSATFabricante;
    JTextField txFabricaFabricante;


    // CUADRO OPCIONES
    MenuOpcion menuOpcion;

    // ITEMS MENU
    JMenuItem itemConfiguracion;
    JMenuItem itemDesconectar;
    JMenuItem itemSalir;

    // DTMS
    DefaultTableModel dtmTienda;
    DefaultTableModel dtmProveedor;
    DefaultTableModel dtmFabricante;

    JDialog adminPasswordDialog;
    JButton btnValidate;
    JPasswordField adminPassword;

    /**
     * Constructor de la clase
     */
    public Vista(){

        iniciarFrame();

    }

    /**
     * Metodo para inciiar los componentes
     */
    private void iniciarFrame(){


        this.setContentPane(panel1);
        this.setTitle("IULIAN PREDA PC-SHOP");
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth()+200,this.getHeight()+100));
        this.setLocationRelativeTo(null);
        menuOpcion = new MenuOpcion(this);

        setMenu();
        setAdminDialog();
        setEnumsComboBox();
        setTableModels();

    }

    /**
     * Metodo para agregar un menu archivo
     */
    private void setMenu(){
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");

        itemConfiguracion = new JMenuItem("Configuracion");
        itemConfiguracion.setActionCommand("Configuracion");

        itemDesconectar = new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");

        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");

        menu.add(itemConfiguracion);
        menu.add(itemDesconectar);
        menu.add(itemSalir);

        menuBar.add(menu);
        menuBar.add(Box.createHorizontalGlue());

        this.setJMenuBar(menuBar);
    }

    /**
     * Metodo para agregar las enumeraciones a los combobox
     * @see CiudadProveedor
     * @see PaisFabricante
     */
    private void setEnumsComboBox(){

       for (CiudadProveedor ciudadProveedor : CiudadProveedor.values()){
            cbCiudadProveedor.addItem(ciudadProveedor.getValor());
        }
        cbCiudadProveedor.setSelectedIndex(-1);

        for (PaisFabricante paisFabricante : PaisFabricante.values()){
            cbPaisFabricante.addItem(paisFabricante.getValor());
        }
        cbPaisFabricante.setSelectedIndex(-1);

    }

    /**
     * Metodo para crear los default table model
     */
    private void setTableModels(){

        this.dtmTienda = new DefaultTableModel();
        this.tablaTienda.setModel(dtmTienda);

        this.dtmProveedor = new DefaultTableModel();
        this.tablaProveedor.setModel(dtmProveedor);

        this.dtmFabricante = new DefaultTableModel();
        this.tablaFabricante.setModel(dtmFabricante);

    }

    /**
     * Metodo para crear el cuadro de contrasenya del administrador junto a la ventana correspondiente.
     */
    private void setAdminDialog() {
        btnValidate = new JButton("OK");
        btnValidate.setActionCommand("Validar");
        adminPassword = new JPasswordField();
        adminPassword.setPreferredSize(new Dimension(100, 26));
        Object[] options = new Object[] {adminPassword, btnValidate};
        JOptionPane jop = new JOptionPane("Introduce la contraseña"
                , JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION, null, options);

        adminPasswordDialog = new JDialog(this, "Opciones", true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(this);

    }
}
