package interfaz;

import util.Util;

import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.Properties;

/**
 * Clase publica modelo, tiene parametros
 */
public class Modelo {

    private String ip;
    private String base;
    private String puerto;
    private String usuario;
    private String passwd;
    private String adminPasswd;

    /**
     * Metodo para coger los datos del archivo properties
     */
    public Modelo() {
        getPropValues();
    }

    /**
     * Metodo para coger el dato de la IP.
     * @return devuelve el valor almacenado en el archivo properties
     */
    public String getIp() {
        return ip;
    }

    /**
     * Metodo para coger el dato de la base
     * @return devuelve el valor almacenado en el archivo properties
     */
    public String getBase() {
        return base;
    }

    /**
     * Metodo para coger el puerto de la base
     * @return devuelve el valor almacenado en el archivo properties
     */
    public String getPuerto() {
        return puerto;
    }

    /**
     * Metodo para coger el usuario
     * @return devuelve el valor almacenado en el archivo properties
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * Metodo para coger la contrasenya del usuario
     * @return devuelve el valor almacenado en el archivo properties
     */
    public String getPasswd() {
        return passwd;
    }

    /**
     * Metodo para coger la contrasenya de administrador
     * @return devuelve el valor almacenado en el archivo properties
     */
    public String getAdminPasswd() {
        return adminPasswd;
    }


    private Connection conexion;

    /**
     * Metodo para realizar la conexión con la base de datos.
     */
    public void conectar() {
        try {
            conexion = DriverManager.getConnection("jdbc:mysql://" + ip + ":" + puerto + "/yulipcshop", usuario, passwd);
        } catch (SQLException sqlException) {
            try {
                conexion = DriverManager.getConnection("jdbc:mysql://" + ip + ":" + puerto + "/", usuario, passwd);

                PreparedStatement statement = null;

                String code = leerFichero();
                String[] query = code.split("--");
                for (String aQuery : query) {
                    statement = conexion.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement != null;
                statement.close();
            } catch (SQLException | IOException e) {
                Util.showErrorAlert("No se ha podido establecer la conexión con la base de datos.");
                System.exit(0);
            }
        }
    }

    /**
     * Metodo para desconectar la base de datos
     */
    public void desconectar() {
        try {
            conexion.close();
            conexion = null;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Metodo para leer el archivo SQL de base de datos
     * @return
     * @throws IOException
     */
    private String leerFichero() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader("src/basedatos.sql"))) {
            String linea;
            StringBuilder stringBuilder = new StringBuilder();

            while ((linea = reader.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }

            return stringBuilder.toString();
        }
    }

    /**
     * Metodo para coger los valores almacenados en el fichero de configuracion properties
     */
    private void getPropValues() {
        InputStream is = null;

        try {
            Properties properties = new Properties();
            String confName = "src/config.properties";

            is = new FileInputStream(confName);

            properties.load(is);

            ip = properties.getProperty("ip");
            base = properties.getProperty("baseDatos");
            puerto = properties.getProperty("puerto");
            usuario = properties.getProperty("usuario");
            passwd = properties.getProperty("password");
            adminPasswd = properties.getProperty("adminPassword");
        } catch (Exception e) {
            System.out.println("Exepción: " + e);
        } finally {
            try {
                if (is != null) is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Metodo para establecer valores de configuracion en el fichero properties
     * @param ip establece la ip a la cual conecta la base de datos
     * @param baseDatos establece la base de datos
     * @param puerto establece el puerto de conexion
     * @param usuario establece el usuario
     * @param passwd establece la contrasenya de conexion
     * @param adminPasswd establece la contrasenya de administrador para abrir el menu de configuracion
     */
    public void setPropValues(String ip, String baseDatos, String puerto, String usuario, String passwd, String adminPasswd) {
        try {
            Properties properties = new Properties();
            properties.setProperty("ip", ip);
            properties.setProperty("baseDatos", baseDatos);
            properties.setProperty("puerto", puerto);
            properties.setProperty("usuario", usuario);
            properties.setProperty("password", passwd);
            properties.setProperty("adminPassword", adminPasswd);
            OutputStream out = new FileOutputStream("src/config.properties");
            properties.store(out, null);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.ip = ip;
        this.base = baseDatos;
        this.puerto = puerto;
        this.usuario = usuario;
        this.passwd = passwd;
    }


    /**
     * Metodo para dar de alta un componente en la tienda
     * @param numSerie
     * @param proveedor
     * @param marcaTienda
     * @param modelo
     * @param procesador
     * @param ram
     * @param placaBase
     * @param precio
     * @param entrada
     */
    void altaTienda(String numSerie, String proveedor, String marcaTienda, String modelo, String procesador, String ram, String placaBase, Float precio, LocalDate entrada) {

        String query = "INSERT INTO tiendas (serial_ordenador, idprov, idfab, modelo, procesador, memoria_ram, placa_base, precio, fecha_entrada) VALUES (?,?,?,?,?,?,?,?,?)";
        PreparedStatement sentencia = null;

        int idprov = Integer.valueOf(proveedor.split(" ")[0]);
        int idfab = Integer.valueOf(marcaTienda.split(" ")[0]);

        try {
            System.out.println("insercion alta");
            sentencia = conexion.prepareStatement(query);
            sentencia.setString(1, numSerie);
            sentencia.setInt(2, idprov);
            sentencia.setInt(3, idfab);
            sentencia.setString(4, modelo);
            sentencia.setString(5, procesador);
            sentencia.setString(6, ram);
            sentencia.setString(7, placaBase);
            sentencia.setFloat(8, precio);
            sentencia.setDate(9, Date.valueOf(entrada));
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            System.out.println("CATCH ALTA TIENDA");
        } finally {
            System.out.println("FINALLY ALTA TIENDA");
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }

    }

    /**
     * Metodo para formatear la base de datos, asignando un nombre personalizado a cada campo
     * @return
     * @throws SQLException
     */
    ResultSet consultaTienda() throws SQLException {

        String query =
                "SELECT concat(t.idtienda) as 'ID'," +
                        "concat(t.serial_ordenador) as 'NUM. DE SERIE'," +
                        "concat(p.idprov, ' - ', p.nombre) as 'PROVEEDOR'," +
                        "concat(f.idfab, ' - ', f.empresa) as 'FABRICANTE'," +
                        "concat(t.modelo) as 'MODELO'," +
                        "concat(t.procesador) as 'PROCESADOR'," +
                        "concat(t.memoria_ram) as 'RAM INSTALADA'," +
                        "concat(t.placa_base) as 'PLACA BASE'," +
                        "concat(t.precio) as 'PRECIO FINAL'," +
                        "concat(fecha_entrada) as 'FECHA DE ALTA'" +
                        " FROM tiendas as t inner join proveedores as p on p.idprov = t.idprov inner join fabricantes as f on f.idfab = t.idfab";

        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(query);
        resultado = sentencia.executeQuery();
        System.out.println("CONSULTA TIENDA");
        return resultado;
    }

    /**
     * Metodo para dar de alta un proveedor
     * @param nombreProveedor
     * @param cifProveedor
     * @param dirProveedor
     * @param ciudadProveedor
     * @param telefonoProveedor
     * @param correoProveedor
     * @param descuentoProveedor
     * @param altaProveedor
     */
    void altaProveedor(String nombreProveedor, String cifProveedor, String dirProveedor, String ciudadProveedor, String telefonoProveedor, String correoProveedor, int descuentoProveedor, LocalDate altaProveedor) {

        String query = "INSERT INTO proveedores (nombre, cif, direccion, ciudad, telefono, email, descuento, proveedor_desde) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            System.out.println("try alta proveedor");
            sentencia = conexion.prepareStatement(query);
            sentencia.setString(1, nombreProveedor);
            sentencia.setString(2, cifProveedor);
            sentencia.setString(3, dirProveedor);
            sentencia.setString(4, ciudadProveedor);
            sentencia.setString(5, telefonoProveedor);
            sentencia.setString(6, correoProveedor);
            sentencia.setInt(7, descuentoProveedor);
            sentencia.setDate(8, Date.valueOf(altaProveedor));
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
           // sqle.printStackTrace();
            Util.showErrorAlert("Este cif ya existe");
            System.out.println("catch alta proveedor");
        } finally {
            System.out.println("finally alta");
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
        System.out.println("Ya no hay try cat ch");
    }

    /**
     * Metodo para formatear la base de datos, asignando un nombre personalizado a cada campo
     * @return
     * @throws SQLException
     */
    ResultSet consultaProveedor() throws SQLException {

        String query =
                "SELECT concat(idprov) as 'ID'," +
                        "concat(nombre) as 'PROVEEDOR'," +
                        "concat(cif) as 'CIF'," +
                        "concat(direccion) as 'DIRECCIÓN'," +
                        "concat(ciudad) as 'CIUDAD'," +
                        "concat(telefono) as 'TELÉFONO'," +
                        "concat(email) as 'eMAIL'," +
                        "concat(descuento) as 'DESCUENTO'," +
                        "concat(proveedor_desde) as 'FECHA DE ALTA' FROM proveedores";

        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(query);
        resultado = sentencia.executeQuery();
        System.out.println("consulta proveedor");
        return resultado;

    }

    /**
     * Metodo para dar de alta un fabricante
     * @param empresaFabricante
     * @param cifFabricante
     * @param paisFabricante
     * @param fabricaFabricante
     * @param tlfFabricante
     * @param mailFabricante
     * @param satFabricante
     */
    void altaFabricante(String empresaFabricante, String cifFabricante, String paisFabricante, String fabricaFabricante, String tlfFabricante, String mailFabricante, String satFabricante) {

        String query = "INSERT INTO fabricantes (empresa,cif,pais_origen,fabrica,tlfcontacto,email,tlfsat) values (?,?,?,?,?,?,?)";
        PreparedStatement sentencia = null;
        System.out.println("alta fabricante");

        try {
            System.out.println("try fabricante");
            sentencia = conexion.prepareStatement(query);
            sentencia.setString(1, empresaFabricante);
            sentencia.setString(2, cifFabricante);
            sentencia.setString(3, paisFabricante);
            sentencia.setString(4, fabricaFabricante);
            sentencia.setString(5, tlfFabricante);
            sentencia.setString(6, mailFabricante);
            sentencia.setString(7, satFabricante);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            Util.showErrorAlert("Este cif ya existe");
            //sqle.printStackTrace();
        } finally {

            System.out.println("finally fabricante");
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }

            }
        }
        System.out.println("ya no hay nada fabricante");
    }

    /**
     * Metodo para formatear la base de datos, asignando un nombre personalizado a cada campo
     * @return
     * @throws SQLException
     */
    ResultSet consultaFabricante() throws SQLException {

        System.out.println("consulta fabricante");
        String query =
                "SELECT concat(idfab) as 'ID'," +
                        "concat(empresa) as 'FABRICANTE'," +
                        "concat(cif) as 'CIF'," +
                        "concat(pais_origen) as 'PAIS'," +
                        "concat(fabrica) as 'FABRICA'," +
                        "concat(tlfcontacto) as 'CONTACTO DIRECTO'," +
                        "concat(email) as 'eMAIL'," +
                        "concat(tlfsat) as 'SAT' FROM fabricantes";

        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(query);
        resultado = sentencia.executeQuery();
        return resultado;
    }


    /**
     * Metodo para modificar los datos previamente introducidos de un proveedor
     * @param nombreProveedor
     * @param cifProveedor
     * @param dirProveedor
     * @param ciudadProveedor
     * @param telefonoProveedor
     * @param correoProveedor
     * @param descuentoProveedor
     * @param altaProveedor
     * @param idprov
     */
    void modificarProveedor(String nombreProveedor, String cifProveedor, String dirProveedor, String ciudadProveedor, String telefonoProveedor, String correoProveedor, int descuentoProveedor, LocalDate altaProveedor, int idprov) {
        String query = "UPDATE proveedores SET nombre = ?, cif=?,direccion=?,ciudad=?,telefono=?,email=?,descuento=?,proveedor_desde=? WHERE idprov =?";

        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(query);
            sentencia.setString(1, nombreProveedor);
            sentencia.setString(2, cifProveedor);
            sentencia.setString(3, dirProveedor);
            sentencia.setString(4, ciudadProveedor);
            sentencia.setString(5, telefonoProveedor);
            sentencia.setString(6, correoProveedor);
            sentencia.setInt(7, descuentoProveedor);
            sentencia.setDate(8, Date.valueOf(altaProveedor));
            sentencia.setInt(9, idprov);
            sentencia.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    /**
     * Metodo para modificar campos previamente introducidos de fabricante
     * @param empresaFabricante
     * @param cifFabricante
     * @param paisFabricante
     * @param fabricaFabricante
     * @param tlfFabricante
     * @param mailFabricante
     * @param satFabricante
     * @param idfab
     */
    public void modificarFabricante(String empresaFabricante, String cifFabricante, String paisFabricante, String fabricaFabricante, String tlfFabricante, String mailFabricante, String satFabricante, int idfab) {
        String query = "UPDATE fabricantes SET empresa =?, cif=?,pais_origen=?,fabrica=?,tlfcontacto=?,email=?,tlfsat=? WHERE idfab =?";

        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(query);
            sentencia.setString(1, empresaFabricante);
            sentencia.setString(2, cifFabricante);
            sentencia.setString(3, paisFabricante);
            sentencia.setString(4, fabricaFabricante);
            sentencia.setString(5, tlfFabricante);
            sentencia.setString(6, mailFabricante);
            sentencia.setString(7, satFabricante);
            sentencia.setInt(8, idfab);
            sentencia.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    /**
     * Metodo para modificar datos en la tienda.
     * @param numSerie
     * @param proveedor
     * @param marcaTienda
     * @param modelo
     * @param procesador
     * @param ram
     * @param placaBase
     * @param precio
     * @param entrada
     * @param idtienda
     */
    void modificarTienda(String numSerie, String proveedor, String marcaTienda, String modelo, String procesador, String ram, String placaBase, Float precio, LocalDate entrada, int idtienda) {

        System.out.println("entra modificar tienda");
        String query = "UPDATE tiendas SET serial_ordenador=?,idprov=?,idfab=?,modelo=?,procesador=?,memoria_ram=?,placa_base=?,precio=?,fecha_entrada=? WHERE idtienda=?";
        PreparedStatement sentencia = null;
        int idprov = Integer.valueOf(proveedor.split(" ")[0]);
        int idfab = Integer.valueOf(marcaTienda.split(" ")[0]);

        try {
            System.out.println("entra modificar tienda try");
            sentencia = conexion.prepareStatement(query);
            sentencia.setString(1, numSerie);
            sentencia.setInt(2, idprov);
            sentencia.setInt(3, idfab);
            sentencia.setString(4, modelo);
            sentencia.setString(5, procesador);
            sentencia.setString(6, ram);
            sentencia.setString(7, placaBase);
            sentencia.setFloat(8, precio);
            sentencia.setDate(9, Date.valueOf(entrada));
            sentencia.setInt(10, idtienda);
            sentencia.executeUpdate();
        } catch (SQLException sqlex) {
            System.out.println("catch tienda modificar");
            sqlex.printStackTrace();
        } finally {
            System.out.println("finally tienda modificar");
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    /**
     * Metodo para borrar un proveedor de la base de datos
     * @param idprov
     */
    void borrarProveedor(int idprov) {
        String sentencia = "DELETE FROM proveedores WHERE idprov=?";
        PreparedStatement stmt = null;

        try {
            stmt = conexion.prepareStatement(sentencia);
            stmt.setInt(1, idprov);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    /**
     * Metodo para borrar un fabricante
     * @param idfab
     */
    void borrarFabricante(int idfab) {
        String sentencia = "DELETE FROM fabricantes WHERE idfab=?";
        PreparedStatement stmt = null;

        try {
            stmt = conexion.prepareStatement(sentencia);
            stmt.setInt(1, idfab);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    /**
     * Metodo para borrar un producto de la tienda
     * @param idtienda
     */
    void borrarTienda(int idtienda) {
        String sentencia = "DELETE FROM tiendas WHERE idtienda=?";
        PreparedStatement stmt = null;

        try {
            stmt = conexion.prepareStatement(sentencia);
            stmt.setInt(1, idtienda);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

}
