package interfaz;

import javax.swing.*;
import java.awt.*;

/**
 * Clase publica de la interfaz del menu de opciones.
 */

public class MenuOpcion extends JDialog {
    private JPanel panel1;
    JTextField txIPConfig;
    JTextField txBaseConfig;
    JTextField txPuertoConfig;
    JTextField txUsuarioConfig;
    JPasswordField txPWDConfig;
    JButton btnGuardar;
    JPasswordField txAdminPWD;
    private Frame owner;


    /**
     * Constructor de la clase
     * @param owner
     */
    public MenuOpcion(Frame owner){
        super(owner,"Configuracion",true);
        this.owner = owner;
        iniciarComponentes();


    }

    /**
     *
     * Inicializa la clase publica JDialog
     * @see JDialog
     */
    private void iniciarComponentes(){
        this.setContentPane(panel1);
        this.panel1.setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth()+200,this.getHeight()));
        this.setLocationRelativeTo(owner);

    }


}
