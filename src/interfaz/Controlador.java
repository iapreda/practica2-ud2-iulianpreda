package interfaz;


import util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

/**
 * Clase publica del controlador, implementa listeners.
 * @see ActionListener
 * @see ItemListener
 * @see ListSelectionListener
 * @see WindowListener
 */

public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener {

    private Modelo modelo;
    private Vista vista;
    boolean actualizar;


    /**
     * Constructor de la clase
     * @param modelo
     * @param vista
     */
    public Controlador(Modelo modelo, Vista vista) {
        this.modelo = modelo;
        this.vista = vista;
        modelo.conectar();
        setOpciones();
        addActionListeners(this);
        addItemListeners(this);
        addWindowListeners(this);
        actualizarGeneral();

    }

    /**
     * Metodo actualizar general. Agrupa otros 3 metodos.
     */
    private void actualizarGeneral() {

        refrescarProveedor();
        refrescarFabricante();
        refrescarTienda();
        actualizar = false;
    }


    /**
     * Metodo para constuir DTM
     * @param rs
     * @see ResultSet
     * @return devuelve DTM
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelFabricante(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();

        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmFabricante.setDataVector(data, columnNames);

        return vista.dtmFabricante;

    }

    /**
     * Metodo para constuir DTM
     * @param rs
     * @see ResultSet
     * @return devuelve DTM
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelProveedor(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();

        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmProveedor.setDataVector(data, columnNames);

        return vista.dtmProveedor;

    }

    /**
     * Metodo para constuir DTM
     * @param rs
     * @see ResultSet
     * @return devuelve DTM
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelTienda(ResultSet rs) throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmTienda.setDataVector(data, columnNames);

        return vista.dtmTienda;
    }

    /**
     * Metodo para añadir datos a vector.
     * @param rs
     * @param columnCount cuenta las columnas
     * @param data
     * @throws SQLException
     */
    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }

    /**
     * Metodo que habilita la interaccion con ventanas
     * @param controlador
     */
    private void addWindowListeners(Controlador controlador) {
        vista.addWindowListener(controlador);
    }

    private void addItemListeners(Controlador controlador) {
    }

    /**
     * Metodo para definir la accion de los botones e items.
     * @param controlador
     */
    private void addActionListeners(Controlador controlador) {
        vista.btnAddTienda.addActionListener(controlador);
        vista.btnEliminarTienda.addActionListener(controlador);
        vista.btnModificarTienda.addActionListener(controlador);

        vista.btnAddFabricante.addActionListener(controlador);
        vista.btnEliminarFabricante.addActionListener(controlador);
        vista.btnModificarFabricante.addActionListener(controlador);

        vista.btnAddProveedor.addActionListener(controlador);
        vista.btnEliminarProveedor.addActionListener(controlador);
        vista.btnModificarProveedor.addActionListener(controlador);

        vista.itemConfiguracion.addActionListener(controlador);
        vista.itemDesconectar.addActionListener(controlador);
        vista.itemSalir.addActionListener(controlador);
        vista.btnValidate.addActionListener(controlador);
        vista.menuOpcion.btnGuardar.addActionListener(controlador);

    }

    /**
     * Metodo para coger los datos de configuracion
     */
    private void setOpciones() {
        vista.menuOpcion.txIPConfig.setText(modelo.getIp());
        vista.menuOpcion.txBaseConfig.setText(modelo.getBase());
        vista.menuOpcion.txPuertoConfig.setText(modelo.getPuerto());
        vista.menuOpcion.txUsuarioConfig.setText(modelo.getUsuario());
        vista.menuOpcion.txPWDConfig.setText(modelo.getPasswd());
    }


    /**
     * Switch case de todo el ejercicio.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        String cmd = e.getActionCommand();


        switch (cmd) {
            case "Configuracion":
                vista.adminPasswordDialog.setVisible(true);
                break;
            case "Desconectar":
                modelo.desconectar();
                break;
            case "Salir":
                System.exit(0);
                break;

            case "Validar":
                if (String.valueOf(vista.adminPassword.getPassword()).equals(modelo.getAdminPasswd())) {
                    vista.adminPassword.setText("");
                    vista.adminPasswordDialog.dispose();
                    vista.menuOpcion.setVisible(true);
                } else {
                    Util.showErrorAlert("La contraseña introducida no es correcta.");
                }
                break;
            case "guardarOpciones":
                modelo.setPropValues(vista.menuOpcion.txIPConfig.getText(),
                        vista.menuOpcion.txBaseConfig.getText(),
                        vista.menuOpcion.txPuertoConfig.getText(),
                        vista.menuOpcion.txUsuarioConfig.getText(),
                        String.valueOf(vista.menuOpcion.txPWDConfig.getPassword()),
                        String.valueOf(vista.menuOpcion.txAdminPWD.getPassword()));
                vista.menuOpcion.dispose();
                vista.dispose();
                new Controlador(new Modelo(), new Vista());
                break;

            case "agregarTienda":
                try {
                    if (comprobarTiendaVacia()) {
                        Util.showErrorAlert("Te has dejado algún campo sin rellenar.");
                        vista.tablaTienda.clearSelection();
                    } else {
                        modelo.altaTienda(
                                vista.txNumSerie.getText(),
                                String.valueOf(vista.cbProveedor.getSelectedItem()),
                                String.valueOf(vista.cbMarcaTienda.getSelectedItem()),
                                vista.txModeloTienda.getText(),
                                vista.txProcesadorTienda.getText(),
                                vista.txRamTienda.getText(),
                                vista.txPlacaTienda.getText(),
                                Float.parseFloat(vista.txPrecioTienda.getText()),
                                vista.dpEntradaTienda.getDate());
                        System.out.println("ALTA TIENDA");
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Comprueba que los datos sean correctos.");
                    vista.tablaTienda.clearSelection();
                }
                borrarCamposTienda();
                refrescarTienda();
                break;

            case "agregarProveedor":
                try {
                    if (comprobarProveedorVacio()) {
                        Util.showErrorAlert("Te has dejado algún campo sin rellenar.");
                        vista.tablaProveedor.clearSelection();
                    } else {
                        modelo.altaProveedor(
                                vista.txNombreProveedor.getText(),
                                vista.txCIFProveedor.getText(),
                                vista.txDirProveedor.getText(),
                                (String) vista.cbCiudadProveedor.getSelectedItem(),
                                vista.txTelefonoProveedor.getText(),
                                vista.txCorreoProveedor.getText(),
                                Integer.parseInt(vista.txDescuentoProveedor.getText()),
                                vista.dpProveedor.getDate());
                        System.out.println("ALTA PROVEEDOR");
                        refrescarProveedor();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Comprueba que los datos sean correctos");
                    vista.tablaProveedor.clearSelection();
                }
                borrarCamposProveedor();
                break;

            case "agregarFabricante":
                try {
                    if (comprobarFabricanteVacio()) {
                        Util.showErrorAlert("Te has dejado algún campo sin rellenar.");
                        vista.tablaFabricante.clearSelection();
                    } else {
                        modelo.altaFabricante(
                                vista.txEmpresaFabricante.getText(),
                                vista.txCIFFabricante.getText(),
                                (String) vista.cbPaisFabricante.getSelectedItem(),
                                vista.txFabricaFabricante.getText(),
                                vista.txTlfFabricante.getText(),
                                vista.txMailFabricante.getText(),
                                vista.txSATFabricante.getText());
                        System.out.println("ALTA FABRICANTE");
                        refrescarFabricante();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Comprueba que los datos sean correctos");
                    vista.tablaFabricante.clearSelection();
                }
                borrarCamposFabricante();
                break;

            case "modificarTienda":
                try {
                    if (comprobarTiendaVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablaTienda.clearSelection();
                    } else {
                        modelo.modificarTienda(
                                vista.txNumSerie.getText(),
                                String.valueOf(vista.cbProveedor.getSelectedItem()),
                                String.valueOf(vista.cbMarcaTienda.getSelectedItem()),
                                vista.txModeloTienda.getText(),
                                vista.txProcesadorTienda.getText(),
                                vista.txRamTienda.getText(),
                                vista.txPlacaTienda.getText(),
                                Float.parseFloat(vista.txPrecioTienda.getText()),
                                vista.dpEntradaTienda.getDate(),
                                Integer.parseInt(String.valueOf(vista.tablaTienda.getValueAt(vista.tablaTienda.getSelectedRow(), 0))));
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tablaTienda.clearSelection();
                }
                borrarCamposTienda();
                refrescarTienda();
                break;
            case "modificarProveedor":
                try {
                    if (comprobarProveedorVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablaProveedor.clearSelection();
                    } else {
                        modelo.modificarProveedor(
                                vista.txNombreProveedor.getText(),
                                vista.txCIFProveedor.getText(),
                                vista.txDirProveedor.getText(),
                                String.valueOf(vista.cbCiudadProveedor.getSelectedItem()),
                                vista.txTelefonoProveedor.getText(),
                                vista.txCorreoProveedor.getText(),
                                Integer.parseInt(vista.txDescuentoProveedor.getText()),
                                vista.dpProveedor.getDate(),
                                Integer.parseInt(String.valueOf(vista.tablaProveedor.getValueAt(vista.tablaProveedor.getSelectedRow(), 0))));

                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tablaProveedor.clearSelection();
                }

                borrarCamposProveedor();
                refrescarProveedor();
                break;

            case "modificarFabricante":
                try {
                    if (comprobarFabricanteVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablaFabricante.clearSelection();
                    } else {
                        modelo.modificarFabricante(
                                vista.txEmpresaFabricante.getText(),
                                vista.txCIFFabricante.getText(),
                                String.valueOf(vista.cbPaisFabricante.getSelectedItem()),
                                vista.txFabricaFabricante.getText(),
                                vista.txTlfFabricante.getText(),
                                vista.txMailFabricante.getText(),
                                vista.txSATFabricante.getText(),
                                Integer.parseInt(String.valueOf(vista.tablaFabricante.getValueAt(vista.tablaFabricante.getSelectedRow(), 0))));

                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tablaFabricante.clearSelection();
                }

                borrarCamposFabricante();
                refrescarFabricante();

            break;

            case "eliminarTienda":
                modelo.borrarTienda(Integer.parseInt((String) vista.tablaTienda.getValueAt(vista.tablaTienda.getSelectedRow(), 0)));
                borrarCamposTienda();
                refrescarTienda();
                break;

            case "eliminarProveedor":
                modelo.borrarProveedor(Integer.parseInt((String)vista.tablaProveedor.getValueAt(vista.tablaProveedor.getSelectedRow(), 0)));
                borrarCamposProveedor();
                refrescarProveedor();
                break;
            case "eliminarFabricante":
                modelo.borrarFabricante(Integer.parseInt((String)vista.tablaFabricante.getValueAt(vista.tablaFabricante.getSelectedRow(), 0)));
                borrarCamposFabricante();
                refrescarFabricante();
                break;
        }


    }

    /**
     * Borra todos los campos del fabricante
     */
    private void borrarCamposFabricante() {
        vista.txEmpresaFabricante.setText("");
        vista.txCIFFabricante.setText("");
        vista.cbPaisFabricante.setSelectedIndex(-1);
        vista.txFabricaFabricante.setText("");
        vista.txTlfFabricante.setText("");
        vista.txMailFabricante.setText("");
        vista.txSATFabricante.setText("");

    }

    /**
     * Actualiza la tabla del fabricante
     */
    private void refrescarFabricante() {
        try {
            vista.tablaFabricante.setModel(construirTableModelFabricante(modelo.consultaFabricante()));
            vista.cbMarcaTienda.removeAllItems();
            for (int i = 0; i < vista.dtmFabricante.getRowCount(); i++) {
                vista.cbMarcaTienda.addItem(vista.dtmFabricante.getValueAt(i, 0) + " - " + vista.dtmFabricante.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Comprueba que no haya campos vacios
     * @return
     */
    private boolean comprobarFabricanteVacio() {
        return vista.txEmpresaFabricante.getText().isEmpty() ||
                vista.txCIFFabricante.getText().isEmpty() ||
                vista.cbPaisFabricante.getSelectedIndex() == -1 ||
                vista.txFabricaFabricante.getText().isEmpty() ||
                vista.txTlfFabricante.getText().isEmpty() ||
                vista.txMailFabricante.getText().isEmpty() ||
                vista.txSATFabricante.getText().isEmpty();

    }

    /**
     * Borra los campos del proveedor
     */
    private void borrarCamposProveedor() {
        vista.txNombreProveedor.setText("");
        vista.txCIFProveedor.setText("");
        vista.txDirProveedor.setText("");
        vista.cbProveedor.setSelectedIndex(-1);
        vista.txTelefonoProveedor.setText("");
        vista.txCorreoProveedor.setText("");
        vista.txDescuentoProveedor.setText("");
        vista.dpProveedor.setText("");
    }

    /**
     * Actualiza la tabla del proveedor
     */
    private void refrescarProveedor() {
        try {
            vista.tablaProveedor.setModel(construirTableModelProveedor(modelo.consultaProveedor()));
            vista.cbProveedor.removeAllItems();
            for (int i = 0; i < vista.dtmProveedor.getRowCount(); i++) {
                vista.cbProveedor.addItem(vista.dtmProveedor.getValueAt(i, 0) + " - " + vista.dtmProveedor.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * Comprueba que no haya campos vacios
     * @return
     */
    private boolean comprobarProveedorVacio() {
        return vista.txNombreProveedor.getText().isEmpty() ||
                vista.txCIFProveedor.getText().isEmpty() ||
                vista.txDirProveedor.getText().isEmpty() ||
                vista.cbCiudadProveedor.getSelectedIndex() == -1 ||
                vista.txTelefonoProveedor.getText().isEmpty() ||
                vista.txCorreoProveedor.getText().isEmpty() ||
                vista.txDescuentoProveedor.getText().isEmpty() ||
                vista.dpProveedor.getText().isEmpty();
    }

    /**
     * Metodo para actualizar la tabla cada vez que se introducen datos nuevos
     */
    private void refrescarTienda() {

        try {
            vista.tablaTienda.setModel(construirTableModelTienda(modelo.consultaTienda()));
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    /**
     * Vacia los campos
     */
    private void borrarCamposTienda() {

        vista.txNumSerie.setText("");
        vista.cbProveedor.setSelectedIndex(-1);
        vista.cbMarcaTienda.setSelectedIndex(-1);
        vista.txModeloTienda.setText("");
        vista.txProcesadorTienda.setText("");
        vista.txRamTienda.setText("");
        vista.txPlacaTienda.setText("");
        vista.txPrecioTienda.setText("");
        vista.dpEntradaTienda.setText("");

    }

    /**
     * Comprueba que no haya campos vacios
     * @return
     */
    private boolean comprobarTiendaVacia() {
        return vista.txNumSerie.getText().isEmpty() ||
                vista.cbProveedor.getSelectedIndex() == -1 ||
                vista.cbMarcaTienda.getSelectedIndex() == -1 ||
                vista.txModeloTienda.getText().isEmpty() ||
                vista.txProcesadorTienda.getText().isEmpty() ||
                vista.txRamTienda.getText().isEmpty() ||
                vista.txPlacaTienda.getText().isEmpty() ||
                vista.txPrecioTienda.getText().isEmpty() ||
                vista.dpEntradaTienda.getText().isEmpty();
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    /**
     * Metodo que comprueba las tablas cada vez que se actualizan.
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {

        if (e.getValueIsAdjusting() && !((ListSelectionModel) e.getSource()).isSelectionEmpty()) {
            if (e.getSource().equals(vista.tablaProveedor.getSelectionModel())) {
                int row = vista.tablaProveedor.getSelectedRow();
                vista.txNombreProveedor.setText(String.valueOf(vista.tablaProveedor.getValueAt(row, 1)));
                vista.txCIFProveedor.setText(String.valueOf(vista.tablaProveedor.getValueAt(row, 2)));
                vista.txDirProveedor.setText(String.valueOf(vista.tablaProveedor.getValueAt(row, 3)));
                vista.cbCiudadProveedor.setSelectedItem(String.valueOf(vista.tablaProveedor.getValueAt(row, 4)));
                vista.txTelefonoProveedor.setText(String.valueOf(vista.tablaProveedor.getValueAt(row, 5)));
                vista.txCorreoProveedor.setText(String.valueOf(vista.tablaProveedor.getValueAt(row, 6)));
                vista.txDescuentoProveedor.setText(String.valueOf(vista.tablaProveedor.getValueAt(row, 7)));
                vista.dpProveedor.setDate((Date.valueOf(String.valueOf(vista.tablaProveedor.getValueAt(row, 8))).toLocalDate()));
            } else if (e.getSource().equals(vista.tablaFabricante.getSelectionModel().isSelectionEmpty())) {
                int row = vista.tablaFabricante.getSelectedRow();
                vista.txEmpresaFabricante.setText(String.valueOf(vista.tablaFabricante.getValueAt(row, 1)));
                vista.txCIFFabricante.setText(String.valueOf(vista.tablaFabricante.getValueAt(row, 2)));
                vista.cbPaisFabricante.setSelectedItem(String.valueOf(vista.tablaFabricante.getValueAt(row, 3)));
                vista.txFabricaFabricante.setText(String.valueOf(vista.tablaFabricante.getValueAt(row, 4)));
                vista.txTlfFabricante.setText(String.valueOf(vista.tablaFabricante.getValueAt(row, 5)));
                vista.txMailFabricante.setText(String.valueOf(vista.tablaFabricante.getValueAt(row, 6)));
                vista.txSATFabricante.setText(String.valueOf(vista.tablaFabricante.getValueAt(row, 7)));

            } else if (e.getSource().equals(vista.tablaTienda.getSelectionModel().isSelectionEmpty())) {
                int row = vista.tablaTienda.getSelectedRow();
                vista.txNumSerie.setText(String.valueOf(vista.tablaTienda.getValueAt(row, 1)));
                vista.cbProveedor.setSelectedItem(String.valueOf(vista.tablaTienda.getValueAt(row, 2)));
                vista.cbMarcaTienda.setSelectedItem(String.valueOf(vista.tablaTienda.getValueAt(row, 3)));
                vista.txModeloTienda.setText(String.valueOf(vista.tablaTienda.getValueAt(row, 4)));
                vista.txProcesadorTienda.setText(String.valueOf(vista.tablaTienda.getValueAt(row, 5)));
                vista.txRamTienda.setText(String.valueOf(vista.tablaTienda.getValueAt(row, 6)));
                vista.txPlacaTienda.setText(String.valueOf(vista.tablaTienda.getValueAt(row, 7)));
                vista.txPrecioTienda.setText(String.valueOf(vista.tablaTienda.getValueAt(row, 8)));
                vista.dpEntradaTienda.setDate((Date.valueOf(String.valueOf(vista.tablaTienda.getValueAt(row, 9))).toLocalDate()));

            } else if (e.getValueIsAdjusting() && ((ListSelectionModel) e.getSource()).isSelectionEmpty() && !actualizar) {
                if (e.getSource().equals(vista.tablaProveedor.getSelectionModel())) {
                    borrarCamposProveedor();
                } else if (e.getSource().equals(vista.tablaFabricante.getSelectionModel())) {
                    borrarCamposFabricante();
                } else if (e.getSource().equals(vista.tablaTienda.getSelectionModel())) {
                    borrarCamposTienda();
                }
            }
        }


    }
}
