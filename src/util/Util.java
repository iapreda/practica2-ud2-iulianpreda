package util;

import javax.swing.*;

public class Util {

    /**
     * Metodo para crear un mensaje de alerta que aparece cuando hay un error.
     * @param mensaje
     */

    public static void showErrorAlert(String mensaje){
        JOptionPane.showMessageDialog(null,mensaje,"Error",JOptionPane.ERROR_MESSAGE);
    }

}
