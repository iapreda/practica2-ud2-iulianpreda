CREATE DATABASE if not exists yulipcshop;
--
USE yulipcshop;
--
create table if not exists tiendas
(

    idtienda         int auto_increment primary key,
    serial_ordenador varchar(35) unique not null,
    idprov           int                not null,
    idfab            int                not null,
    modelo           varchar(30)        not null,
    procesador       varchar(45)        not null,
    memoria_ram      varchar(20)        not null,
    placa_base       varchar(30)        not null,
    precio           float              not null,
    fecha_entrada    date

);
--
create table IF NOT EXISTS proveedores
(

    idprov          int auto_increment primary key,
    nombre          varchar(30)        not null,
    cif             varchar(10) unique not null,
    direccion       varchar(40)        not null,
    ciudad          varchar(30)        not null,
    telefono        varchar(9)         not null,
    email           varchar(40)        not null,
    descuento       int,
    proveedor_desde date

);
--
create table if not exists fabricantes
(

    idfab       int auto_increment primary key,
    empresa     varchar(30)        not null,
    cif         varchar(10) unique not null,
    pais_origen varchar(35)        not null,
    fabrica     varchar(25)        not null,
    tlfcontacto varchar(15),
    email       varchar(30),
    tlfsat      varchar(15)

);
--
alter table tiendas
    add foreign key (idprov) references proveedores (idprov),
    add foreign key (idfab) references fabricantes (idfab);
--

